using Unity.Entities;
using Unity.Mathematics;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct UfoSpawningData : IComponentData
    {
        public float2 UfoSpeedMinMax;
    }
}