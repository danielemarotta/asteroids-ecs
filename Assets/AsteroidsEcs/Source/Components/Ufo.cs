using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct Ufo : IComponentData
    {
        public int ScoreReward;

        public float ChangeDirectionChance;

        public float ChangeDirectionCooldown;
    }
}