using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

namespace AsteroidsEcs
{
    public class PowerUpCollisionSystem : CollisionSystem<PowerUpCollisionSystem.TriggerJob>
    {
        protected override TriggerJob PrepareJob(EntityCommandBuffer commandBuffer)
        {
            return new TriggerJob
            {
                CommandBuffer = commandBuffer,
                PlayerFromEntity = GetComponentDataFromEntity<Player>(true),
                PowerUpFromEntity = GetComponentDataFromEntity<PowerUp>(true),
                InvulnerabilityFromEntity = GetComponentDataFromEntity<Invulnerability>(true),
                WeaponBufferFromEntity = GetBufferFromEntity<Weapon>()
            };
        }

        [BurstCompile]
        public struct TriggerJob : ITriggerEventsJob
        {
            public EntityCommandBuffer CommandBuffer;

            [ReadOnly] public ComponentDataFromEntity<Player> PlayerFromEntity;

            [ReadOnly] public ComponentDataFromEntity<PowerUp> PowerUpFromEntity;

            [ReadOnly] public ComponentDataFromEntity<Invulnerability> InvulnerabilityFromEntity;

            public BufferFromEntity<Weapon> WeaponBufferFromEntity;

            public void Execute(TriggerEvent triggerEvent)
            {
                var entityA = triggerEvent.EntityA;
                var entityB = triggerEvent.EntityB;

                var entityAIsPlayer = PlayerFromEntity.HasComponent(entityA);
                if ((entityAIsPlayer && PowerUpFromEntity.HasComponent(entityB)) ||
                    (PlayerFromEntity.HasComponent(entityB) && PowerUpFromEntity.HasComponent(entityB)))
                {
                    var playerEntity = entityAIsPlayer ? entityA : entityB;
                    var powerUpEntity = entityAIsPlayer ? entityB : entityA;

                    var powerUp = PowerUpFromEntity[powerUpEntity];
                    CommandBuffer.DestroyEntity(powerUpEntity);

                    switch (powerUp.PowerUpType)
                    {
                        case PowerUpType.Invulnerability:
                            var invulnerability = InvulnerabilityFromEntity[playerEntity];
                            invulnerability.Duration = math.max(powerUp.Duration, invulnerability.Duration);
                            CommandBuffer.SetComponent(playerEntity, invulnerability);
                            break;

                        case PowerUpType.RocketPack:
                            var weaponBuffer = WeaponBufferFromEntity[playerEntity];
                            var secondaryWeapon = weaponBuffer[1];
                            secondaryWeapon.Ammo += powerUp.Amount;
                            weaponBuffer[1] = secondaryWeapon;
                            break;
                    }
                }
            }
        }
    }
}