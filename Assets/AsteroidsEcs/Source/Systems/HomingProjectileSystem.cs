using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class HomingProjectileSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var targetsQuery = GetEntityQuery(typeof(Translation), typeof(HomingProjectileTarget));
            if (targetsQuery.CalculateEntityCount() == 0) return;

            var targets = targetsQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var dt = Time.DeltaTime;

            Entities.WithReadOnly(targets).WithDisposeOnCompletion(targets).ForEach(
                (ref MovementData movementData, in Translation translation, in HomingProjectile homingProjectile) =>
                {
                    var closestTarget = float3.zero;

                    var minDistanceSq = float.PositiveInfinity;
                    for (var i = 0; i < targets.Length; i++)
                    {
                        var target = targets[i];

                        var distanceSq = math.lengthsq(target.Value - translation.Value);
                        if (distanceSq < minDistanceSq)
                        {
                            minDistanceSq = distanceSq;
                            closestTarget = target.Value;
                        }
                    }

                    var projectileSpeed = math.length(movementData.Velocity);
                    var projectileDirection = math.lerp(
                        movementData.Velocity / projectileSpeed,
                        math.normalize(closestTarget - translation.Value),
                        homingProjectile.LerpToTargetFactor * dt);
                    projectileDirection = math.normalize(projectileDirection);
                    movementData.Velocity = projectileDirection * projectileSpeed;
                }).ScheduleParallel();
        }
    }
}