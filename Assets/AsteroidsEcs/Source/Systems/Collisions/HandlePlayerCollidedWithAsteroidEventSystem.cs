using Unity.Entities;

namespace AsteroidsEcs
{
    public class HandlePlayerCollidedWithAsteroidEventSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            Entities.WithAll<Player, CollidedWithAsteroidEvent>().ForEach(
                (Entity entity) =>
                {
                    commandBuffer.AddComponent<PlayerDeathEvent>(commandBuffer.CreateEntity());
                    commandBuffer.RemoveComponent<CollidedWithAsteroidEvent>(entity);
                }).Schedule();
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}