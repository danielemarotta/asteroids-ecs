using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct UfoRandomMovementData : IComponentData
    {
        [HideInInspector] public float ChangeDirectionCooldown;
    }
}