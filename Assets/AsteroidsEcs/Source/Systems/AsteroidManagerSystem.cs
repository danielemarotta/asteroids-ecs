using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    public class AsteroidManagerSystem : SystemBase
    {
        private EntityQuery _asteroidQuery;

        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _asteroidQuery = GetEntityQuery(ComponentType.ReadOnly<Asteroid>());
            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (_asteroidQuery.CalculateEntityCount() > 0) return;

            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();

            var random = new Random((uint)Environment.TickCount);

            var asteroidManager = GetSingletonEntity<AsteroidSpawningData>();
            var spawningData = GetComponent<AsteroidSpawningData>(asteroidManager);
            var prefabs = GetBuffer<PrefabElement>(asteroidManager).Reinterpret<Entity>();
            var movementDataFromEntity = GetComponentDataFromEntity<MovementData>();

            var playAreaSize = GetSingleton<GameConfig>().PlayAreaSize;
            var safeArea = spawningData.SafeArea;
            var positionRandomness = playAreaSize * 0.5f - safeArea;

            Job.WithCode(
                () =>
                {
                    var asteroidCount = random.NextInt(spawningData.AsteroidCountMinMax.x, spawningData.AsteroidCountMinMax.y + 1);
                    for (var i = 0; i < asteroidCount; i++)
                    {
                        var asteroidPrefab = prefabs[random.NextInt(0, prefabs.Length)];

                        var position = float3.zero;
                        var sign = random.NextFloat() < 0.5f ? 1 : -1;
                        if (random.NextFloat() < 0.5f)
                        {
                            position.x = (safeArea.x + random.NextFloat() * positionRandomness.x) * sign;
                            position.y = playAreaSize.y * random.NextFloat();
                        }
                        else
                        {
                            position.x = playAreaSize.x * random.NextFloat();
                            position.y = (safeArea.y + random.NextFloat() * positionRandomness.y) * sign;
                        }

                        var wrapAround = movementDataFromEntity[asteroidPrefab].WrapsAround;
                        var velocity = RandomUtility.RandomVelocity(random, spawningData.AsteroidSpeedMinMax.x, spawningData.AsteroidSpeedMinMax.y);

                        var asteroid = commandBuffer.Instantiate(asteroidPrefab);
                        commandBuffer.SetComponent(asteroid, new Translation { Value = position });
                        commandBuffer.SetComponent(asteroid, new Rotation { Value = RandomUtility.RandomRotation(random) });
                        commandBuffer.SetComponent(asteroid, new MovementData { WrapsAround = wrapAround, Velocity = velocity });
                    }
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}