using Unity.Entities;

namespace AsteroidsEcs
{
    public class TimerSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities.ForEach((ref TimerData timerData) => { timerData.Value -= dt; }).ScheduleParallel();
        }
    }
}