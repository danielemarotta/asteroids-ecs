using Unity.Entities;

namespace AsteroidsEcs
{
    public enum PowerUpType
    {
        Invulnerability,

        RocketPack
    }

    [GenerateAuthoringComponent]
    public struct PowerUp : IComponentData
    {
        public PowerUpType PowerUpType;

        public float Duration;

        public int Amount;
    }
}