using Unity.Entities;
using Unity.Mathematics;

namespace AsteroidsEcs
{
    public class ClampVelocitySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach(
                (ref MovementData movementData, in ClampVelocity clampVelocity) =>
                {
                    var velocityMagnitude = math.length(movementData.Velocity);
                    if (velocityMagnitude > 0)
                        movementData.Velocity = movementData.Velocity / velocityMagnitude * math.min(velocityMagnitude, clampVelocity.MaxSpeed);
                }).ScheduleParallel();
        }
    }
}