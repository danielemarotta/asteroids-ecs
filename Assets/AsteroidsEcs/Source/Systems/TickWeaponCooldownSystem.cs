using Unity.Entities;
using Unity.Mathematics;

namespace AsteroidsEcs
{
    public class TickWeaponCooldownSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities.ForEach(
                (ref DynamicBuffer<Weapon> weapons) =>
                {
                    for (var i = 0; i < weapons.Length; i++)
                    {
                        var weapon = weapons[i];
                        weapon.Cooldown = math.max(weapon.Cooldown - dt, 0);
                        weapons[i] = weapon;
                    }
                }).ScheduleParallel();
        }
    }
}