using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    public class WeaponAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        public List<WeaponDefinition> Definitions;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var buffer = dstManager.AddBuffer<Weapon>(entity);
            foreach (var definition in Definitions)
            {
                var weapon = new Weapon
                {
                    FireRate = definition.FireRate,
                    ProjectilePrefab = conversionSystem.GetPrimaryEntity(definition.ProjectilePrefab),
                    Ammo = definition.InfiniteAmmo ? int.MaxValue : 0
                };
                buffer.Add(weapon);
            }
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            foreach (var definition in Definitions) referencedPrefabs.Add(definition.ProjectilePrefab);
        }
    }
}