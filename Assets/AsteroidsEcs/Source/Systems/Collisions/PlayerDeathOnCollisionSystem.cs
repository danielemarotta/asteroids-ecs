using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;

namespace AsteroidsEcs
{
    public class PlayerDeathOnCollisionSystem<T> : CollisionSystem<PlayerDeathOnCollisionSystem<T>.TriggerJob>
        where T : struct, IComponentData
    {
        protected override TriggerJob PrepareJob(EntityCommandBuffer commandBuffer)
        {
            return new TriggerJob
            {
                PlayerFromEntity = GetComponentDataFromEntity<Player>(true),
                OtherFromEntity = GetComponentDataFromEntity<T>(true),
                CommandBuffer = commandBuffer
            };
        }

        [BurstCompile]
        public struct TriggerJob : ITriggerEventsJob
        {
            public EntityCommandBuffer CommandBuffer;

            [ReadOnly] public ComponentDataFromEntity<Player> PlayerFromEntity;

            [ReadOnly] public ComponentDataFromEntity<T> OtherFromEntity;

            public void Execute(TriggerEvent triggerEvent)
            {
                var entityA = triggerEvent.EntityA;
                var entityB = triggerEvent.EntityB;

                var entityAIsPlayer = PlayerFromEntity.HasComponent(entityA);
                if ((entityAIsPlayer && OtherFromEntity.HasComponent(entityB)) ||
                    (PlayerFromEntity.HasComponent(entityB) && OtherFromEntity.HasComponent(entityB)))
                {
                    CommandBuffer.DestroyEntity(entityAIsPlayer ? entityB : entityA);
                    CommandBuffer.AddComponent<PlayerDeathEvent>(CommandBuffer.CreateEntity());
                }
            }
        }
    }
}