using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class PlayerDeathEventSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (!TryGetSingletonEntity<PlayerDeathEvent>(out var playerDeathEventEntity)) return;

            var gameConfig = GetSingleton<GameConfig>();
            var gameStateEntity = GetSingletonEntity<GameState>();
            var gameState = GetSingleton<GameState>();

            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();

            Entities.WithAll<Player>().ForEach(
                (
                    Entity entity,
                    ref Translation playerTranslation,
                    ref Rotation playerRotation,
                    ref MovementData playerMovementData,
                    ref Invulnerability playerInvulnerability) =>
                {
                    commandBuffer.DestroyEntity(playerDeathEventEntity);

                    if (playerInvulnerability.Duration > 0) return;

                    if (--gameState.LifeCount > 0)
                    {
                        playerTranslation.Value = float3.zero;
                        playerRotation.Value = quaternion.identity;
                        playerMovementData.Acceleration = float3.zero;
                        playerInvulnerability.Duration = gameConfig.InvulnerabilityDurationOnDeath;
                    }
                    else
                    {
                        commandBuffer.DestroyEntity(entity);
                    }

                    commandBuffer.SetComponent(gameStateEntity, gameState);
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}