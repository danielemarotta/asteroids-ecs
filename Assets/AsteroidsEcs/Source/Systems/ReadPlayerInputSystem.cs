using Unity.Entities;
using UnityEngine.InputSystem;

namespace AsteroidsEcs
{
    public class ReadPlayerInputSystem : SystemBase
    {
        private Controls _controls;

        private bool _hyperspaceActionStarted;

        protected override void OnCreate()
        {
            base.OnCreate();

            _controls = new Controls();
            _controls.Enable();
            _controls.Game.Hyperspace.started += OnHyperspaceStarted;
        }

        protected override void OnDestroy()
        {
            _controls.Game.Hyperspace.started -= OnHyperspaceStarted;
            base.OnDestroy();
        }

        private void OnHyperspaceStarted(InputAction.CallbackContext callbackContext)
        {
            _hyperspaceActionStarted = true;
        }

        protected override void OnUpdate()
        {
            Entities.WithAll<Player>().ForEach(
                (ref PlayerInput playerInput) =>
                {
                    var actions = _controls.Game;
                    playerInput.Thrust = actions.Thrust.ReadValue<float>() > 0;
                    playerInput.Rotate = actions.Rotate.ReadValue<float>();
                    playerInput.FirePrimaryWeapon = actions.FirePrimaryWeapon.ReadValue<float>() > 0;
                    playerInput.FireSecondaryWeapon = actions.FireSecondaryWeapon.ReadValue<float>() > 0;
                    playerInput.Hyperspace = _hyperspaceActionStarted;
                    _hyperspaceActionStarted = false;
                }).WithoutBurst().Run();
        }
    }
}