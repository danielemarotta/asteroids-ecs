using Unity.Entities;
using Unity.Mathematics;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct AsteroidSpawningData : IComponentData
    {
        public int2 AsteroidCountMinMax;

        public float2 AsteroidSpeedMinMax;

        public float2 SafeArea;
    }
}