using System;
using Unity.Entities;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    public class UfoRandomMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            var random = new Random((uint)Environment.TickCount);

            Entities.ForEach(
                (ref UfoRandomMovementData ufoRandomMovementData, ref MovementData movementData, in Ufo ufo) =>
                {
                    ufoRandomMovementData.ChangeDirectionCooldown -= dt;
                    if (ufoRandomMovementData.ChangeDirectionCooldown > 0) return;

                    ufoRandomMovementData.ChangeDirectionCooldown = ufo.ChangeDirectionCooldown;

                    if (random.NextFloat() < ufo.ChangeDirectionChance)
                    {
                        var currentSpeed = math.length(movementData.Velocity);
                        var currentDirection = movementData.Velocity / currentSpeed;

                        if (math.abs(currentDirection.y) < math.EPSILON)
                            currentDirection.y = random.NextFloat() < 0.5f ? -1 : 1;
                        else
                            currentDirection.y = 0;
                        movementData.Velocity = currentSpeed * math.normalize(currentDirection);
                    }
                }).ScheduleParallel();
        }
    }
}