using Unity.Entities;

namespace AsteroidsEcs
{
    public class HandleProjectileCollidedWithAsteroidEventSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            Entities.WithAll<Projectile>().ForEach(
                (Entity projectile, in CollidedWithAsteroidEvent e) =>
                {
                    commandBuffer.DestroyEntity(projectile);
                    commandBuffer.AddComponent(commandBuffer.CreateEntity(), new AddToScore { Value = e.ScoreReward });
                }).Schedule();
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}