using Unity.Entities;

namespace AsteroidsEcs
{
    public struct PrefabElement : IBufferElementData
    {
        public Entity Prefab;
    }
}