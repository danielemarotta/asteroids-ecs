using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct HomingProjectile : IComponentData
    {
        public float LerpToTargetFactor;
    }
}