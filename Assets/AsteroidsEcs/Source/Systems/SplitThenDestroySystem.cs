using System;
using Unity.Entities;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    public class SplitThenDestroySystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var spawningData = GetSingleton<AsteroidSpawningData>();
            var movementDataFromEntity = GetComponentDataFromEntity<MovementData>();
            var random = new Random((uint)Environment.TickCount);

            Entities.WithAll<SplitThenDestroy>().ForEach(
                (Entity entity, ref DynamicBuffer<PrefabElement> prefabs, in Translation translation) =>
                {
                    for (var i = 0; i < 2; i++)
                    {
                        var asteroidPrefab = prefabs[random.NextInt(prefabs.Length)].Prefab;
                        var asteroid = commandBuffer.Instantiate(asteroidPrefab);
                        commandBuffer.SetComponent(asteroid, translation);
                        commandBuffer.SetComponent(asteroid, new Rotation { Value = RandomUtility.RandomRotation(random) });
                        var wrapsAround = movementDataFromEntity[asteroidPrefab].WrapsAround;
                        var velocity = RandomUtility.RandomVelocity(random, spawningData.AsteroidSpeedMinMax.x, spawningData.AsteroidSpeedMinMax.y);
                        commandBuffer.SetComponent(asteroid, new MovementData { WrapsAround = wrapsAround, Velocity = velocity });
                    }
                }).Schedule();
            Entities.WithAll<SplitThenDestroy>().ForEach((Entity entity) => commandBuffer.DestroyEntity(entity)).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}