using Unity.Entities;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class AttachToSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var translationFromEntity = GetComponentDataFromEntity<Translation>();

            Entities.ForEach(
                (Entity entity, in AttachTo attachTo) =>
                {
                    if (attachTo.Target == Entity.Null) return;
                    commandBuffer.SetComponent(entity, translationFromEntity[attachTo.Target]);
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}