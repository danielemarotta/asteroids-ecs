using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;

namespace AsteroidsEcs
{
    public abstract class AsteroidCollisionSystem<T> : CollisionSystem<AsteroidCollisionSystem<T>.TriggerJob>
        where T : struct, IComponentData
    {
        protected override TriggerJob PrepareJob(EntityCommandBuffer commandBuffer)
        {
            return new TriggerJob
            {
                AsteroidFromEntity = GetComponentDataFromEntity<Asteroid>(true),
                OtherFromEntity = GetComponentDataFromEntity<T>(true),
                CommandBuffer = commandBuffer
            };
        }

        [BurstCompile]
        public struct TriggerJob : ITriggerEventsJob
        {
            public EntityCommandBuffer CommandBuffer;

            [ReadOnly] public ComponentDataFromEntity<Asteroid> AsteroidFromEntity;

            [ReadOnly] public ComponentDataFromEntity<T> OtherFromEntity;

            public void Execute(TriggerEvent triggerEvent)
            {
                var entityA = triggerEvent.EntityA;
                var entityB = triggerEvent.EntityB;

                if (AsteroidFromEntity.HasComponent(entityA) && OtherFromEntity.HasComponent(entityB))
                    HandleEntityCollidingWithAsteroid(entityA, AsteroidFromEntity[entityA].ScoreReward, entityB);
                else if (AsteroidFromEntity.HasComponent(entityB) && OtherFromEntity.HasComponent(entityA))
                    HandleEntityCollidingWithAsteroid(entityB, AsteroidFromEntity[entityB].ScoreReward, entityA);
            }

            private void HandleEntityCollidingWithAsteroid(Entity asteroid, int scoreReward, Entity other)
            {
                CommandBuffer.AddComponent<SplitThenDestroy>(asteroid);
                CommandBuffer.AddComponent(other, new CollidedWithAsteroidEvent { ScoreReward = scoreReward });
            }
        }
    }
}