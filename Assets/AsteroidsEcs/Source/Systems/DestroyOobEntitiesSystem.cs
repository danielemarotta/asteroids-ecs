using Unity.Entities;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class DestroyOobEntitiesSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var halfPlayAreaSize = 0.5f * GetSingleton<GameConfig>().PlayAreaSize;
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();

            Entities.WithAll<DestroyWhenOob>().ForEach(
                (Entity entity, in Translation translation) =>
                {
                    var position = translation.Value;
                    if (position.x < -1 * halfPlayAreaSize.x ||
                        position.x > halfPlayAreaSize.x ||
                        position.y < -1 * halfPlayAreaSize.y ||
                        position.y > halfPlayAreaSize.y)
                        commandBuffer.DestroyEntity(entity);
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}