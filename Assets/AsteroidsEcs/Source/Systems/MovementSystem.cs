using Unity.Entities;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class MovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            var playAreaSize = GetSingleton<GameConfig>().PlayAreaSize;
            var halfPlayAreaSize = 0.5f * playAreaSize;

            Entities.ForEach(
                (ref Translation translation, ref MovementData movementData) =>
                {
                    movementData.Velocity += movementData.Acceleration * dt;

                    var translationValue = translation.Value;
                    translationValue += movementData.Velocity * dt;

                    if (movementData.WrapsAround)
                    {
                        if (translationValue.x > halfPlayAreaSize.x) translationValue.x -= playAreaSize.x;
                        if (translationValue.x < -1 * halfPlayAreaSize.x) translationValue.x += playAreaSize.x;
                        if (translationValue.y > halfPlayAreaSize.y) translationValue.y -= playAreaSize.y;
                        if (translationValue.y < -1 * halfPlayAreaSize.y) translationValue.y += playAreaSize.y;
                    }

                    translation.Value = translationValue;
                }).ScheduleParallel();
        }
    }
}