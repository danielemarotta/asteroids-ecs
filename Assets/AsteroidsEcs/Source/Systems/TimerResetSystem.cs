using System;
using Unity.Entities;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class TimerResetSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var random = new Random((uint)Environment.TickCount);
            Entities.ForEach(
                (ref TimerData timerData) =>
                {
                    if (timerData.Value <= 0) timerData.Value = random.NextFloat(timerData.RandomTimerRange.x, timerData.RandomTimerRange.y);
                }).ScheduleParallel();
        }
    }
}