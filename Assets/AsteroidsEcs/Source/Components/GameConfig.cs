using Unity.Entities;
using Unity.Mathematics;

namespace AsteroidsEcs
{
    public struct GameConfig : IComponentData
    {
        public Entity PlayerPrefab;

        public int InitialLifeCount;

        public float2 PlayAreaSize;

        public float InvulnerabilityDurationOnDeath;
    }
}