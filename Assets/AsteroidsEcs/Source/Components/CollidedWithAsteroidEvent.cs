using Unity.Entities;

namespace AsteroidsEcs
{
    public struct CollidedWithAsteroidEvent : IComponentData
    {
        public int ScoreReward;
    }
}