using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace AsteroidsEcs
{
    [UpdateAfter(typeof(ReadPlayerInputSystem))]
    public class ApplyPlayerInputSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities.ForEach(
                (ref MovementData movementData, ref Rotation rotation, in PlayerInput playerInput) =>
                {
                    rotation.Value = math.mul(
                        rotation.Value,
                        quaternion.RotateZ(math.radians(-1 * playerInput.RotationSpeed) * playerInput.Rotate * dt));

                    movementData.Acceleration =
                        playerInput.Thrust ? math.rotate(rotation.Value, new float3(0, playerInput.ThrustPower, 0)) : float3.zero;
                }).ScheduleParallel();
        }
    }
}