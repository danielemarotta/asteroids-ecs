using Unity.Entities;

namespace AsteroidsEcs
{
    public class HandleUfoCollidedWithAsteroidEventSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            Entities.WithAll<Ufo, CollidedWithAsteroidEvent>().ForEach((Entity entity) => { commandBuffer.DestroyEntity(entity); }).Schedule();
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}