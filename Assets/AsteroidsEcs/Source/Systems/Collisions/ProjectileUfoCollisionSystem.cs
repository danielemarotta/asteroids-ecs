using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;

namespace AsteroidsEcs
{
    public class ProjectileUfoCollisionSystem : CollisionSystem<ProjectileUfoCollisionSystem.TriggerJob>
    {
        protected override TriggerJob PrepareJob(EntityCommandBuffer commandBuffer)
        {
            return new TriggerJob
            {
                ProjectileFromEntity = GetComponentDataFromEntity<Projectile>(true),
                UfoFromEntity = GetComponentDataFromEntity<Ufo>(true),
                CommandBuffer = commandBuffer
            };
        }

        [BurstCompile]
        public struct TriggerJob : ITriggerEventsJob
        {
            public EntityCommandBuffer CommandBuffer;

            [ReadOnly] public ComponentDataFromEntity<Projectile> ProjectileFromEntity;

            [ReadOnly] public ComponentDataFromEntity<Ufo> UfoFromEntity;

            public void Execute(TriggerEvent triggerEvent)
            {
                var entityA = triggerEvent.EntityA;
                var entityB = triggerEvent.EntityB;

                var entityAIsUfo = UfoFromEntity.HasComponent(entityA);
                var entityBIsUfo = UfoFromEntity.HasComponent(entityB);

                if ((ProjectileFromEntity.HasComponent(entityA) && entityBIsUfo) || (ProjectileFromEntity.HasComponent(entityB) && entityAIsUfo))
                {
                    CommandBuffer.DestroyEntity(entityA);
                    CommandBuffer.DestroyEntity(entityB);
                    var scoreReward = UfoFromEntity[entityAIsUfo ? entityA : entityB].ScoreReward;
                    CommandBuffer.AddComponent(CommandBuffer.CreateEntity(), new AddToScore { Value = scoreReward });
                }
            }
        }
    }
}