using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace AsteroidsEcs
{
    public class GameUiController : MonoBehaviour
    {
        [SerializeField] private TMP_Text _scoreText;

        [SerializeField] private TMP_Text _lifeCountText;

        [SerializeField] private TMP_Text _rocketCountText;

        [SerializeField] private Canvas _gameOverPanel;

        private void LateUpdate()
        {
            var entityManager = World.All[0].EntityManager;

            var gameState = entityManager.CreateEntityQuery(typeof(GameState)).GetSingleton<GameState>();
            var playerQuery = entityManager.CreateEntityQuery(typeof(Player));
            var isGameOver = playerQuery.CalculateEntityCount() == 0;

            _scoreText.text = gameState.Score.ToString("000000");
            _lifeCountText.text = gameState.LifeCount.ToString();
            if (!isGameOver)
            {
                var rocketCount = entityManager.GetBuffer<Weapon>(playerQuery.GetSingletonEntity())[1].Ammo;
                _rocketCountText.text = rocketCount.ToString();
            }

            _gameOverPanel.enabled = isGameOver;
            if (isGameOver && Keyboard.current.anyKey.wasPressedThisFrame)
                entityManager.AddComponent<StartOrRestartGameEvent>(entityManager.CreateEntity());
        }
    }
}