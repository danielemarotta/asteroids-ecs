// GENERATED AUTOMATICALLY FROM 'Assets/AsteroidsEcs/Data/Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace AsteroidsEcs
{
    public class @Controls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @Controls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""Controls"",
    ""maps"": [
        {
            ""name"": ""Game"",
            ""id"": ""c795d2bf-645b-4da3-ab65-2f363b3dbd0f"",
            ""actions"": [
                {
                    ""name"": ""Rotate"",
                    ""type"": ""Value"",
                    ""id"": ""36905a74-48a7-4bce-ae16-deded0812691"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Thrust"",
                    ""type"": ""Button"",
                    ""id"": ""d3cc7259-5b76-47ed-8f14-8a00c28da05a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire Primary Weapon"",
                    ""type"": ""Button"",
                    ""id"": ""00bc4069-9858-4103-99ce-ef0fb55f7f1a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire Secondary Weapon"",
                    ""type"": ""Button"",
                    ""id"": ""f52be04a-c9d5-4cc9-9ad4-34d3f6283923"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Hyperspace"",
                    ""type"": ""Button"",
                    ""id"": ""5dff1204-5643-4b83-a69e-ff8996594c9a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Left / Right"",
                    ""id"": ""a157f6e9-e6e6-44bc-92eb-3d2fc1fd9db8"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ea7923b6-84ee-4602-9fa0-eb7e5f56e7e4"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""10510a17-80a1-4b59-a0e1-ad36c57eca66"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""A / D"",
                    ""id"": ""6dd8b54a-1870-4f18-855a-6c22783d39b9"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""f8ae8bab-febf-4fa8-a997-6eaab2eb236f"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""d198c5e3-6753-42f9-800a-c192faaf0c62"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Rotate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""3ed5b4d2-97f6-499e-aea0-977705a3ff78"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee2ef6a9-e2bf-4b45-b175-f75110b0575b"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Thrust"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""50f77329-6334-4de4-b63c-856dd2a7cec0"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire Primary Weapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3215eeef-49c0-4ee7-98fa-73a169959c08"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Hyperspace"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3c1728ff-31bd-46c3-ba38-1ba521db09af"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire Secondary Weapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Game
            m_Game = asset.FindActionMap("Game", throwIfNotFound: true);
            m_Game_Rotate = m_Game.FindAction("Rotate", throwIfNotFound: true);
            m_Game_Thrust = m_Game.FindAction("Thrust", throwIfNotFound: true);
            m_Game_FirePrimaryWeapon = m_Game.FindAction("Fire Primary Weapon", throwIfNotFound: true);
            m_Game_FireSecondaryWeapon = m_Game.FindAction("Fire Secondary Weapon", throwIfNotFound: true);
            m_Game_Hyperspace = m_Game.FindAction("Hyperspace", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Game
        private readonly InputActionMap m_Game;
        private IGameActions m_GameActionsCallbackInterface;
        private readonly InputAction m_Game_Rotate;
        private readonly InputAction m_Game_Thrust;
        private readonly InputAction m_Game_FirePrimaryWeapon;
        private readonly InputAction m_Game_FireSecondaryWeapon;
        private readonly InputAction m_Game_Hyperspace;
        public struct GameActions
        {
            private @Controls m_Wrapper;
            public GameActions(@Controls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Rotate => m_Wrapper.m_Game_Rotate;
            public InputAction @Thrust => m_Wrapper.m_Game_Thrust;
            public InputAction @FirePrimaryWeapon => m_Wrapper.m_Game_FirePrimaryWeapon;
            public InputAction @FireSecondaryWeapon => m_Wrapper.m_Game_FireSecondaryWeapon;
            public InputAction @Hyperspace => m_Wrapper.m_Game_Hyperspace;
            public InputActionMap Get() { return m_Wrapper.m_Game; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(GameActions set) { return set.Get(); }
            public void SetCallbacks(IGameActions instance)
            {
                if (m_Wrapper.m_GameActionsCallbackInterface != null)
                {
                    @Rotate.started -= m_Wrapper.m_GameActionsCallbackInterface.OnRotate;
                    @Rotate.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnRotate;
                    @Rotate.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnRotate;
                    @Thrust.started -= m_Wrapper.m_GameActionsCallbackInterface.OnThrust;
                    @Thrust.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnThrust;
                    @Thrust.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnThrust;
                    @FirePrimaryWeapon.started -= m_Wrapper.m_GameActionsCallbackInterface.OnFirePrimaryWeapon;
                    @FirePrimaryWeapon.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnFirePrimaryWeapon;
                    @FirePrimaryWeapon.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnFirePrimaryWeapon;
                    @FireSecondaryWeapon.started -= m_Wrapper.m_GameActionsCallbackInterface.OnFireSecondaryWeapon;
                    @FireSecondaryWeapon.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnFireSecondaryWeapon;
                    @FireSecondaryWeapon.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnFireSecondaryWeapon;
                    @Hyperspace.started -= m_Wrapper.m_GameActionsCallbackInterface.OnHyperspace;
                    @Hyperspace.performed -= m_Wrapper.m_GameActionsCallbackInterface.OnHyperspace;
                    @Hyperspace.canceled -= m_Wrapper.m_GameActionsCallbackInterface.OnHyperspace;
                }
                m_Wrapper.m_GameActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Rotate.started += instance.OnRotate;
                    @Rotate.performed += instance.OnRotate;
                    @Rotate.canceled += instance.OnRotate;
                    @Thrust.started += instance.OnThrust;
                    @Thrust.performed += instance.OnThrust;
                    @Thrust.canceled += instance.OnThrust;
                    @FirePrimaryWeapon.started += instance.OnFirePrimaryWeapon;
                    @FirePrimaryWeapon.performed += instance.OnFirePrimaryWeapon;
                    @FirePrimaryWeapon.canceled += instance.OnFirePrimaryWeapon;
                    @FireSecondaryWeapon.started += instance.OnFireSecondaryWeapon;
                    @FireSecondaryWeapon.performed += instance.OnFireSecondaryWeapon;
                    @FireSecondaryWeapon.canceled += instance.OnFireSecondaryWeapon;
                    @Hyperspace.started += instance.OnHyperspace;
                    @Hyperspace.performed += instance.OnHyperspace;
                    @Hyperspace.canceled += instance.OnHyperspace;
                }
            }
        }
        public GameActions @Game => new GameActions(this);
        public interface IGameActions
        {
            void OnRotate(InputAction.CallbackContext context);
            void OnThrust(InputAction.CallbackContext context);
            void OnFirePrimaryWeapon(InputAction.CallbackContext context);
            void OnFireSecondaryWeapon(InputAction.CallbackContext context);
            void OnHyperspace(InputAction.CallbackContext context);
        }
    }
}
