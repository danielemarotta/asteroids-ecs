using Unity.Entities;

namespace AsteroidsEcs
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class CreateStartOrRestartGameEventOnceSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            _commandBufferSystem = World.GetOrCreateSystem<EndInitializationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            Job.WithCode(() => commandBuffer.AddComponent<StartOrRestartGameEvent>(commandBuffer.CreateEntity())).Schedule();
            _commandBufferSystem.AddJobHandleForProducer(Dependency);

            Enabled = false;
        }
    }
}