using Unity.Entities;

namespace AsteroidsEcs
{
    public class AddToScoreSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var gameStateSingleton = GetSingletonEntity<GameState>();
            var gameState = GetComponent<GameState>(gameStateSingleton);
            Entities.ForEach((in AddToScore addToScore) => { gameState.Score += addToScore.Value; }).Run();
            SetComponent(gameStateSingleton, gameState);

            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            Entities.WithAll<AddToScore>().ForEach((Entity entity) => commandBuffer.DestroyEntity(entity)).Schedule();
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}