using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct TimerData : IComponentData
    {
        public float2 RandomTimerRange;

        [HideInInspector] public float Value;
    }
}