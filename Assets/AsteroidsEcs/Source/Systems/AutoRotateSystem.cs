using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace AsteroidsEcs
{
    public class AutoRotateSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities.ForEach(
                (ref Rotation rotation, in AutoRotate autoRotate) => rotation.Value = math.mul(
                    rotation.Value,
                    quaternion.Euler(0, 0, math.radians(autoRotate.RotationSpeedInDegrees * dt)))).ScheduleParallel();
        }
    }
}