using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    public class UfoFireSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var playerQuery = GetEntityQuery(typeof(Player));
            if (playerQuery.CalculateEntityCount() == 0) return;

            var playerEntity = playerQuery.GetSingletonEntity();
            var target = GetComponent<Translation>(playerEntity).Value;

            var random = new Random((uint)Environment.TickCount);
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var projectileFromEntity = GetComponentDataFromEntity<Projectile>();

            Entities.WithAll<Ufo>().ForEach(
                (ref DynamicBuffer<Weapon> ufoWeapons, in FiringAccuracy ufoAccuracy, in Translation ufoTranslation) =>
                {
                    // We assume ufos have a single weapon
                    var weapon = ufoWeapons[0];
                    if (weapon.Cooldown > 0) return;
                    weapon.Cooldown = 1f / weapon.FireRate;
                    ufoWeapons[0] = weapon;

                    var toTarget = target - ufoTranslation.Value;
                    var angle = math.atan2(toTarget.y, toTarget.x);
                    angle += random.NextFloat(-0.5f, 0.5f) * (1 - ufoAccuracy.Value) * 2 * math.PI;
                    var velocity = new float3(math.cos(angle), math.sin(angle), 0) * projectileFromEntity[weapon.ProjectilePrefab].Speed;

                    var projectile = commandBuffer.Instantiate(weapon.ProjectilePrefab);
                    commandBuffer.SetComponent(projectile, ufoTranslation);
                    commandBuffer.SetComponent(projectile, new Rotation { Value = quaternion.LookRotation(velocity, new float3(0, 0, 1)) });
                    commandBuffer.SetComponent(projectile, new MovementData { Velocity = velocity });
                }).Schedule();
        }
    }
}