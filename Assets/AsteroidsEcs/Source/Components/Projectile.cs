using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct Projectile : IComponentData
    {
        public float Speed;
    }
}