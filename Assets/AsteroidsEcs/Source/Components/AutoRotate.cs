using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct AutoRotate : IComponentData
    {
        public float RotationSpeedInDegrees;
    }
}