using Unity.Entities;

namespace AsteroidsEcs
{
    public struct Weapon : IBufferElementData
    {
        public Entity ProjectilePrefab;

        public float FireRate;

        public float Cooldown;

        public int Ammo;
    }
}