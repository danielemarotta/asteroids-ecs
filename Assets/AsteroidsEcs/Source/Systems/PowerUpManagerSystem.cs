using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    [UpdateAfter(typeof(TimerSystem))]
    public class PowerUpManagerSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (GetEntityQuery(typeof(Player)).CalculateEntityCount() == 0) return;

            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var random = new Random((uint)Environment.TickCount);
            var halfPlayAreaSize = 0.5f * GetSingleton<GameConfig>().PlayAreaSize;

            Entities.ForEach(
                (in TimerData timerData, in PowerUpSpawningData spawningData, in DynamicBuffer<PrefabElement> prefabs) =>
                {
                    if (timerData.Value > 0) return;

                    var powerUpPrefab = prefabs[random.NextInt(0, prefabs.Length)].Prefab;
                    var powerUp = commandBuffer.Instantiate(powerUpPrefab);

                    var position = new float3(random.NextFloat(-1, 1) * halfPlayAreaSize.x, random.NextFloat(-1, 1) * halfPlayAreaSize.y, 0);
                    commandBuffer.SetComponent(powerUp, new Translation { Value = position });
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}