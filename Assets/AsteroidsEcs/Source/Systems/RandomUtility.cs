using System.Runtime.CompilerServices;
using Unity.Mathematics;

// ReSharper disable PossiblyImpureMethodCallOnReadonlyVariable

namespace AsteroidsEcs
{
    public static class RandomUtility
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float RandomAngle(Random random)
        {
            return random.NextFloat() * 2 * math.PI;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static quaternion RandomRotation(Random random)
        {
            return quaternion.Euler(0, 0, RandomAngle(random));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float3 RandomVelocity(Random random, float min, float max)
        {
            var angle = RandomAngle(random);
            var magnitude = random.NextFloat(min, max);
            return new float3(math.cos(angle), math.sin(angle), 0) * magnitude;
        }
    }
}