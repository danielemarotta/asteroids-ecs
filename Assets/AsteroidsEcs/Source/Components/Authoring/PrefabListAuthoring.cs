using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    public class PrefabListAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        public List<GameObject> Prefabs;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var buffer = dstManager.AddBuffer<PrefabElement>(entity).Reinterpret<Entity>();
            foreach (var prefab in Prefabs) buffer.Add(conversionSystem.GetPrimaryEntity(prefab));
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.AddRange(Prefabs);
        }
    }
}