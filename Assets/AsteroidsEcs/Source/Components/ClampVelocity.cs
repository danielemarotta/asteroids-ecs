using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct ClampVelocity : IComponentData
    {
        public float MaxSpeed;
    }
}