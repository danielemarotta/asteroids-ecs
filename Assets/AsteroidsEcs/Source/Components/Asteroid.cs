using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct Asteroid : IComponentData
    {
        public int ScoreReward;
    }
}