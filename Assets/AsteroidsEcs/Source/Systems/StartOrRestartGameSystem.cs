using Unity.Entities;

namespace AsteroidsEcs
{
    public class StartOrRestartGameSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (!TryGetSingletonEntity<StartOrRestartGameEvent>(out var startOrRestartGameEvent)) return;

            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var gameConfig = GetSingleton<GameConfig>();
            var gameStateEntity = GetSingletonEntity<GameState>();

            Entities.WithAll<DestroyOnGameOver>().ForEach((Entity entity) => commandBuffer.DestroyEntity(entity)).Schedule();
            Entities.WithAll<TimerData>().ForEach((ref TimerData timerData) => timerData.Value = float.NegativeInfinity).ScheduleParallel();
            Job.WithCode(
                () =>
                {
                    commandBuffer.SetComponent(gameStateEntity, new GameState { Score = 0, LifeCount = gameConfig.InitialLifeCount });
                    commandBuffer.Instantiate(gameConfig.PlayerPrefab);
                    commandBuffer.DestroyEntity(startOrRestartGameEvent);
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}