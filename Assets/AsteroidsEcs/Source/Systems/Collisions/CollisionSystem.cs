using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;

namespace AsteroidsEcs
{
    public abstract class CollisionSystem<TJob, TCommandBufferSystem> : SystemBase
        where TJob : struct, ITriggerEventsJob
        where TCommandBufferSystem : EntityCommandBufferSystem
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        private StepPhysicsWorld _stepPhysicsWorld;

        private BuildPhysicsWorld _buildPhysicsWorld;

        protected override void OnCreate()
        {
            _commandBufferSystem = World.GetOrCreateSystem<TCommandBufferSystem>();
            _stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
            _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        }

        protected override void OnUpdate()
        {
            var job = PrepareJob(_commandBufferSystem.CreateCommandBuffer());
            Dependency = job.Schedule(_stepPhysicsWorld.Simulation, ref _buildPhysicsWorld.PhysicsWorld, Dependency);
            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        protected abstract TJob PrepareJob(EntityCommandBuffer commandBuffer);
    }

    public abstract class CollisionSystem<TJob> : CollisionSystem<TJob, EndSimulationEntityCommandBufferSystem>
        where TJob : struct, ITriggerEventsJob
    {
    }
}