using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    public class UfoManagerSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (GetEntityQuery(typeof(Player)).CalculateEntityCount() == 0) return;

            var dt = Time.DeltaTime;
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var random = new Random((uint)Environment.TickCount);
            var movementDataFromEntity = GetComponentDataFromEntity<MovementData>();
            var playAreaSize = GetSingleton<GameConfig>().PlayAreaSize;

            Entities.ForEach(
                (in TimerData timerData, in UfoSpawningData spawningData, in DynamicBuffer<PrefabElement> prefabs) =>
                {
                    if (timerData.Value > 0) return;

                    var ufoPrefab = prefabs[random.NextInt(0, prefabs.Length)].Prefab;
                    var ufo = commandBuffer.Instantiate(ufoPrefab);

                    var side = random.NextFloat() < 0.5f ? 1 : -1;
                    var position = new float3(side * 0.5f * playAreaSize.x, random.NextFloat() * playAreaSize.y, 0);
                    commandBuffer.SetComponent(ufo, new Translation { Value = position });

                    commandBuffer.SetComponent(ufo, new Rotation { Value = RandomUtility.RandomRotation(random) });

                    var wrapAround = movementDataFromEntity[ufoPrefab].WrapsAround;
                    var velocity = new float3(side * random.NextFloat(spawningData.UfoSpeedMinMax.x, spawningData.UfoSpeedMinMax.y), 0, 0);
                    commandBuffer.SetComponent(ufo, new MovementData { WrapsAround = wrapAround, Velocity = velocity });
                }).Schedule();

            _commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}