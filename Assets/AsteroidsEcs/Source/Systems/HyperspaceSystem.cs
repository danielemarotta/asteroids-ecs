using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Random = Unity.Mathematics.Random;

namespace AsteroidsEcs
{
    [UpdateAfter(typeof(ReadPlayerInputSystem))]
    public class HyperspaceSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var random = new Random((uint)Environment.TickCount);
            var halfPlayAreaSize = 0.5f * GetSingleton<GameConfig>().PlayAreaSize;

            Entities.ForEach(
                (ref Translation translation, in PlayerInput playerInput) =>
                {
                    if (playerInput.Hyperspace)
                    {
                        var x = random.NextFloat(-1 * halfPlayAreaSize.x, halfPlayAreaSize.x);
                        var y = random.NextFloat(-1 * halfPlayAreaSize.y, halfPlayAreaSize.y);
                        translation.Value = new float3(x, y, 0);
                    }
                }).ScheduleParallel();
        }
    }
}