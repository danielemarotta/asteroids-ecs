using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct PlayerInput : IComponentData
    {
        public float ThrustPower;

        public float RotationSpeed;

        [HideInInspector] public bool Thrust;

        [HideInInspector] public float Rotate;

        [HideInInspector] public bool FirePrimaryWeapon;

        [HideInInspector] public bool FireSecondaryWeapon;

        [HideInInspector] public bool Hyperspace;
    }
}