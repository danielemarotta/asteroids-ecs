using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct GameState : IComponentData
    {
        [HideInInspector] public int Score;

        [HideInInspector] public int LifeCount;
    }
}