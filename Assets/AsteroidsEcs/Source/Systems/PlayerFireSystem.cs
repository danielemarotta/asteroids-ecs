using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace AsteroidsEcs
{
    [UpdateAfter(typeof(ReadPlayerInputSystem))]
    public class PlayerFireSystem : SystemBase
    {
        private EntityCommandBufferSystem _commandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();

            _commandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var commandBuffer = _commandBufferSystem.CreateCommandBuffer();
            var projectileFromEntity = GetComponentDataFromEntity<Projectile>();

            Entities.ForEach(
                (
                    ref DynamicBuffer<Weapon> weapons,
                    in Translation playerTranslation,
                    in Rotation playerRotation,
                    in MovementData playerMovementData,
                    in PlayerInput playerInput) =>
                {
                    var weaponIndex = playerInput.FirePrimaryWeapon ? 0 : playerInput.FireSecondaryWeapon ? 1 : -1;
                    if (weaponIndex >= 0)
                    {
                        var weapon = weapons[weaponIndex];
                        if (weapon.Cooldown > 0 || weapon.Ammo == 0) return;
                        if (weapon.Ammo < int.MaxValue) weapon.Ammo--;
                        weapon.Cooldown = 1f / weapon.FireRate;
                        weapons[weaponIndex] = weapon;

                        var projectile = commandBuffer.Instantiate(weapon.ProjectilePrefab);
                        commandBuffer.SetComponent(projectile, playerTranslation);
                        commandBuffer.SetComponent(projectile, playerRotation);
                        var velocity = playerMovementData.Velocity +
                                       math.rotate(playerRotation.Value, new float3(0, projectileFromEntity[weapon.ProjectilePrefab].Speed, 0));
                        commandBuffer.SetComponent(projectile, new MovementData { Velocity = velocity });
                    }
                }).Schedule();
        }
    }
}