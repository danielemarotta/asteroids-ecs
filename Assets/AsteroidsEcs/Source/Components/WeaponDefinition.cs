﻿using UnityEngine;

namespace AsteroidsEcs
{
    [CreateAssetMenu(menuName = "Asteroids Ecs/Weapon Definition")]
    public class WeaponDefinition : ScriptableObject
    {
        [SerializeField] private GameObject _projectilePrefab;

        [SerializeField] private float _fireRate;

        [SerializeField] private bool _infiniteAmmo;

        public GameObject ProjectilePrefab => _projectilePrefab;

        public float FireRate => _fireRate;

        public bool InfiniteAmmo => _infiniteAmmo;
    }
}