using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct MovementData : IComponentData
    {
        public bool WrapsAround;

        [HideInInspector] public float3 Acceleration;

        [HideInInspector] public float3 Velocity;
    }
}