using Unity.Entities;

namespace AsteroidsEcs
{
    public struct AddToScore : IComponentData
    {
        public int Value;
    }
}