using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace AsteroidsEcs
{
    public class GameConfigAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
    {
        public GameObject PlayerPrefab;

        public int InitialLifeCount = 3;

        public float InvulnerabilityDurationOnDeath = 3;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var mainCamera = Camera.main;
            var playAreaHeight = mainCamera.orthographicSize * 2;
            var playAreaWidth = mainCamera.aspect * playAreaHeight;

            dstManager.AddComponent<GameConfig>(entity);
            dstManager.SetComponentData(
                entity,
                new GameConfig
                {
                    PlayerPrefab = conversionSystem.GetPrimaryEntity(PlayerPrefab),
                    InitialLifeCount = InitialLifeCount,
                    PlayAreaSize = new Vector2(playAreaWidth, playAreaHeight),
                    InvulnerabilityDurationOnDeath = InvulnerabilityDurationOnDeath
                });
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.Add(PlayerPrefab);
        }
    }
}