using Unity.Entities;

namespace AsteroidsEcs
{
    public struct AttachTo : IComponentData
    {
        public Entity Target;
    }
}