using Unity.Entities;

namespace AsteroidsEcs
{
    [GenerateAuthoringComponent]
    public struct FiringAccuracy : IComponentData
    {
        public float Value;
    }
}